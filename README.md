# wails getting started

wailsのgetting startedをやってみる

* [wailsapp/wails: Create desktop apps using Go and Web Technologies.](https://github.com/wailsapp/wails)
* [Getting Started :: Wails](https://wails.app/gettingstarted/)

## 環境
* Windows 10 64bit Pro
* go version go1.14.4 windows/amd64
* gcc.exe (tdm64-1) 5.1.0
* npm 6.14.6
* wails v1.7.1

## wails cmd
```
$ wails setup
 _       __      _ __    
| |     / /___ _(_) /____
| | /| / / __ `/ / / ___/
| |/ |/ / /_/ / / (__  )  v1.7.1
|__/|__/\__,_/_/_/____/   https://wails.app
The lightweight framework for web-like apps

Welcome to Wails!

Wails is a lightweight framework for creating web-like desktop apps in Go.
I'll need to ask you a few questions so I can fill in your project templates and then I will try and see if you have the correct dependencies installed. If you don't have the right tools installed
, I'll try and suggest how to install them.

What is your name: tsuchinaga
What is your email address: woocun@gmail.com

Wails config saved to: C:\Users\woocu\.wails\wails.json
Feel free to customise these settings.

Detected Platform: Windows
Checking for prerequisites...
Program 'gcc' found: C:\TDM-GCC-64\bin\gcc.exe
Program 'npm' found: C:\Program Files\nodejs\npm.cmd

> Installing Mewn asset packer...
Ready for take off!
Create your first project by running 'wails init'.



$ wails init
Wails v1.7.1 - Initialising project

The name of the project (My Project): wails-getting-started
Project Name: wails-getting-started
The output binary name (wails-getting-started):
Output binary Name: wails-getting-started
Project directory name (wails-getting-started):
Project Directory: wails-getting-started
Please select a template:
  1: Angular - Angular 8 template (Requires node 10.8+)
  2: React JS - Create React App v3 template
  3: Vanilla - A Vanilla HTML/JS template
  4: Vue2/Webpack Basic - A basic Vue2/WebPack4 template
  5: Vuetify1.5/Webpack Basic - A basic Vuetify1.5/Webpack4 template
  6: Vuetify2/Webpack Basic - A basic Vuetify2/Webpack4 template
Please choose an option [1]: 3
Template: Vanilla
> Generating project...
> Building project (this may take a while)...
Project 'wails-getting-started' built in directory 'wails-getting-started'!


$ wails build
Wails v1.7.1 - Building Application

> Skipped frontend dependencies (-f to force rebuild)
> Building frontend...
> Ensuring Dependencies are up to date...
> Packing + Compiling project...
Awesome! Project 'wails-getting-started' built!
```
